import { Injectable } from '@angular/core';
import { Hero } from './../classes/hero';
import { Http, Headers, Response, RequestOptions} from '@angular/http'
import 'rxjs/add/operator/toPromise';

@Injectable()
export class HeroService {

heroes:Hero[];

  constructor (private http:Http){
    
  }

  getHeroes(): Promise<Hero[]> {     
    return this.http.get("http://www.winelove.club/heroes/list.php")
        .toPromise()
        .then (this.extractData)
        .catch (this.handleError);
  }
  

  private handleError(error: Response): Promise<any> {
    return Promise.reject(error);  
  }


  private extractData(res: Response): Promise<Hero[]> {
    return res.json();
  }

  public delete(hero: Hero) : Promise<Hero[]> {     
    let headers = new Headers({ 'Content-Type': 'text/plain' });
    let options = new RequestOptions({ headers: headers });
      return this.http.get("http://www.winelove.club/heroes/delete.php?id=" +  hero.id , options)
          .toPromise()
          .then (this.extractData)
          .catch (this.handleError);
  }
  
/* wait 3 seconds */
  public getHeroesSlowly(): Promise<Hero[]> {
    return new Promise(resolve => {        
        setTimeout(resolve(
          this.getHeroes()
        ),3000);
    });
  }

}
